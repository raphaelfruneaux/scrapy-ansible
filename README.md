# Ansible configuration for Scrapy

Repository consists ansible tasks for install Scrapy framework in Vagrant environment

**Install requirements**

* Midnight Commander
* Vim
* Wget
* Curl
* PIP
* Scrapy
* MongoDB 3
* pymongo

## Using

Clone repository to project_root/roles

Add the next Vagrantfile to project_root

```ruby

# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|

  # virtual machine base box
  config.vm.box = "ubuntu/trusty64"
  
  # forwarding web-server
  config.vm.network "forwarded_port", guest: 27017 , host: 27017
  
  config.vm.synced_folder "./spiders", "/var/spiders", owner: "vagrant", group: "www-data", mount_options: ["dmode=775,fmode=664"], create: true
  
  config.ssh.forward_agent = true
  
  # additional params for virtual machine
  config.vm.provider :virtualbox do |vb|
    vb.customize ["modifyvm", :id, "--memory", "2048"]
    vb.customize ["modifyvm", :id, "--cpus", "2"]   
  end
  
  # configuration for provision
  config.vm.provision :ansible do |ansible|
    ansible.playbook = "./roles/playbook.yml"
    ansible.sudo = true
    #ansible.verbose = "vvv"
    ansible.raw_ssh_args = "-o ControlMaster=no"
    ansible.host_key_checking = false
    ansible.extra_vars = { 
      ansible_ssh_user: 'vagrant'
    }
  end
end

```

Then you should type: `vagrant up --provision`

Add your scrapy spiders to project_root/spiders directory

**Enjoy it!**